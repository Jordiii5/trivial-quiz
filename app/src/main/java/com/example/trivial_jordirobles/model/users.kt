package com.example.trivial_jordirobles.model

data class User(var name:String, var bestScore: Int)