package com.example.trivial_jordirobles.view

import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.trivial_jordirobles.R
import com.example.trivial_jordirobles.databinding.FragmentGameVideojuegosBinding
import com.example.trivial_jordirobles.model.QuestionsLists.arteiLiteraturaQ
import com.example.trivial_jordirobles.model.QuestionsLists.cienciaQ
import com.example.trivial_jordirobles.model.QuestionsLists.cineQ
import com.example.trivial_jordirobles.model.QuestionsLists.deporteQ
import com.example.trivial_jordirobles.model.QuestionsLists.geografiaQ
import com.example.trivial_jordirobles.model.QuestionsLists.historiaQ
import com.example.trivial_jordirobles.model.QuestionsLists.musicaQ
import com.example.trivial_jordirobles.model.QuestionsLists.videojuegosQ
import com.example.trivial_jordirobles.view.CategoriesFragment.Companion.topic
import com.example.trivial_jordirobles.view.GameModesFragment.Companion.gameMode
import com.example.trivial_jordirobles.viewmodel.gameViewModel

class GameFragment : Fragment(), View.OnClickListener {

    lateinit var binding: FragmentGameVideojuegosBinding

    companion object {
        var totalScore = 0
        var round = 0
        var lives = 0
        val allQuestions = mutableListOf(
            deporteQ,
            musicaQ,
            videojuegosQ,
            cienciaQ,
            historiaQ,
            arteiLiteraturaQ,
            geografiaQ,
            cineQ
        )
        var question: List<MutableList<String>> = mutableListOf()
        var livesList = mutableListOf<ImageView?>()
        var randomQuestionsOrder = allQuestions.shuffled()
        var randTopic = 0
        var contadorpreguntascorrectas = 0
        var contadorpreguntasincorrectas = 0
        var answer: List<MutableList<String>> = mutableListOf()
    }

    // BARRA DE CUENTA ATRAS
    val timer = object : CountDownTimer(30000, 100) {
        override fun onTick(millisUntilFinished: Long) {
            binding.countDownProgressBar.progress -= 1
        }
        override fun onFinish() {
            showCorrect()
            delay(1300)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        randomQuestionsOrder = allQuestions.shuffled()
        totalScore = 0
        round = 0
        contadorpreguntascorrectas = 0
        contadorpreguntasincorrectas = 0
        super.onCreate(savedInstanceState)
        binding = FragmentGameVideojuegosBinding.inflate((layoutInflater))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userViewModel = ViewModelProvider(requireActivity()).get(gameViewModel::class.java) //
        lives = -1
        livesList = mutableListOf(binding.live1, binding.live2, binding.live3)
        question = mutableListOf()
        answer = mutableListOf()
        when (gameMode){
            1 -> when (topic) {
                1 -> {
                    question = allQuestions.random().shuffled()
                    binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.preguntasrondaaleatorio)
                }

                2 -> {
                    question = deporteQ.shuffled()
                    binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.rondapreguntasdeporte)
                }
                3 -> {
                    question = musicaQ.shuffled()
                    binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.rondapreguntasmusica)
                }
                4 -> {
                    question = videojuegosQ.shuffled()
                    binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.rondapreguntasvideojuegos)
                }
                5 -> {
                    question = cienciaQ.shuffled()
                    binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.rondapreguntasciencia)
                }
                6 -> {
                    question = historiaQ.shuffled()
                    binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.rondapreguntashistoria)
                }
                7 -> {
                    question = arteiLiteraturaQ.shuffled()
                    binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.rondapreguntasarte)
                }
                8 -> {
                    question = geografiaQ.shuffled()
                    binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.rondapreguntasgeografia)
                }
                9 -> {
                    question = cineQ.shuffled()
                    binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.rondapreguntascine)
                }
            }
            2 -> {
                question = randomQuestionsOrder[5]
                livesList.forEachIndexed() { index, value ->
                    value!!.isVisible = true
                }
                binding.rondapreguntasvideojuegos1.setImageResource(R.drawable.rondapreguntassupervivencia)
            }
        }
        updateInfo()
        binding.respuesta1.setOnClickListener(this)
        binding.respuesta2.setOnClickListener(this)
        binding.respuesta3.setOnClickListener(this)
        binding.respuesta4.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val button = v as Button
        val tag = button.tag.toString().toInt()
        if (question[round][tag].split("_")[0] == "c" || question[round][2] == button.text) {
            disableButton()
            totalScore += binding.countDownProgressBar.progress
            binding.puntuacion.text = "Puntuació: $totalScore"
            button.setBackgroundResource(R.drawable.boton_respuesta_correcta)
            contadorpreguntascorrectas++
        }
        else {
            button.setBackgroundResource(R.drawable.boton_respuesta_incorrecta)
            contadorpreguntasincorrectas++
            showCorrect()
            if (gameMode == 2){
                lives++
                livesList[lives]!!.isVisible = false
            }
        }
        timer.cancel()
        delay(1300)
    }

    fun showCorrect() {
        disableButton()
        if (questionType()) {
            if (question[round][1].split("_")[0] == "c") binding.respuesta1.setBackgroundResource(R.drawable.boton_respuesta_correcta)
            if (question[round][2].split("_")[0] == "c") binding.respuesta2.setBackgroundResource(R.drawable.boton_respuesta_correcta)
            if (question[round][3].split("_")[0] == "c") binding.respuesta3.setBackgroundResource(R.drawable.boton_respuesta_correcta)
            if (question[round][4].split("_")[0] == "c") binding.respuesta4.setBackgroundResource(R.drawable.boton_respuesta_correcta)
        }
        else {
            if (question[round][2] == binding.respuesta2.text) binding.respuesta2.setBackgroundResource(R.drawable.boton_respuesta_correcta)
            if (question[round][3] == binding.respuesta3.text) binding.respuesta3.setBackgroundResource(R.drawable.boton_respuesta_correcta)
        }
    }

    fun updateInfo() {
        binding.countDownProgressBar.progress = 100
        if (gameMode != 2) {
            binding.ronda.text = "${round + 1}/10"
            binding.puntuacion.text = "Puntuación: $totalScore"
        }
        else {
            binding.countDownProgressBar.isVisible = false
            binding.ronda.text = "${round + 1}/15"
            binding.puntuacion.isVisible = false
        }

        if (questionType()) {
            binding.respuesta1.isInvisible = false
            binding.respuesta4.isInvisible = false
            binding.respuesta1.text = question[round][1].split("_")[1]
            binding.respuesta2.text = question[round][2].split("_")[1]
            binding.respuesta3.text = question[round][3].split("_")[1]
            binding.respuesta4.text = question[round][4].split("_")[1]
        } else {
            binding.respuesta1.isInvisible = true
            binding.respuesta2.text = "VERDADERO"
            binding.respuesta3.text = "FALSO"
            binding.respuesta4.isInvisible = true
        }
        binding.respuesta1.isEnabled = true
        binding.respuesta2.isEnabled = true
        binding.respuesta3.isEnabled = true
        binding.respuesta4.isEnabled = true
        binding.pregunta.text = question[round][0]
        binding.respuesta1.setBackgroundResource(R.drawable.boton_jugar_todo_redondo)
        binding.respuesta2.setBackgroundResource(R.drawable.boton_jugar_todo_redondo)
        binding.respuesta3.setBackgroundResource(R.drawable.boton_jugar_todo_redondo)
        binding.respuesta4.setBackgroundResource(R.drawable.boton_jugar_todo_redondo)
        if (gameMode == 2) timer.cancel()
        else timer.start()
    }

    fun questionType(): Boolean {
        return question[round][1] != "VF"
    }

    fun disableButton() {
        binding.respuesta1.isEnabled = false
        binding.respuesta2.isEnabled = false
        binding.respuesta3.isEnabled = false
        binding.respuesta4.isEnabled = false
    }

    fun delay(miliSec: Int) {
        timer.cancel()
        round++
        Handler(Looper.getMainLooper()).postDelayed({

            if (round == 10 && gameMode != 2) changeFragment()
            else if (gameMode == 2 && (lives == 2 || round == 60)) changeFragment()
            else if (gameMode == 2 && round != 0 &&  randTopic != 5){
                if (round ==10 && randTopic != 5) {
                    randTopic++

                }
                if (round == 10){
                    question = randomQuestionsOrder[randTopic]
                    round = 0
                }
                updateInfo()
            }
            else if (randTopic == 5 && round == 10) changeFragment()
            else updateInfo()
        }, miliSec.toLong())
    }

    fun changeFragment() {
        timer.cancel()
        round = 0
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, FragmentResult())
            setReorderingAllowed(true)
            addToBackStack("name")
            commit()
        }
    }
}
