package com.example.trivial_jordirobles.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.trivial_jordirobles.R
import com.example.trivial_jordirobles.databinding.FragmentGameVideojuegosBinding
import com.example.trivial_jordirobles.databinding.FragmentGamemodesBinding
import com.example.trivial_jordirobles.view.GameFragment.Companion.question

class GameModesFragment : Fragment() {
    companion object{
        var gameMode = 0
    }

    lateinit var binding: FragmentGamemodesBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentGamemodesBinding.inflate(layoutInflater)
        question = mutableListOf()
        val anim = AnimationUtils.loadAnimation(context,R.anim.inversetotate_animation)
        val fadeAnim = AnimationUtils.loadAnimation(context,R.anim.fade_in)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        question = mutableListOf()
        binding.gamemode1Button.setOnClickListener {
            gameMode = 1
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, CategoriesFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.gamemode2Button.setOnClickListener {
            gameMode = 2
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }

        binding.goBackButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, HomeFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }


    }
}
