package com.example.trivial_jordirobles.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.trivial_jordirobles.R

class GameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, HomeFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }
    }
    @Override
    override fun onBackPressed() {
        //super.onBackPressed()
    }
}