package com.example.trivial_jordirobles.view

import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.example.trivial_jordirobles.R
import com.example.trivial_jordirobles.databinding.FragmentRankingBinding
import java.io.BufferedReader
import java.io.InputStreamReader

class RankingFragment : Fragment() {
    lateinit var binding: FragmentRankingBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentRankingBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val appContext = requireContext().applicationContext

        var orderedRanking = readFile().split(";") as MutableList<String>
        orderedRanking.removeAt(orderedRanking.size-1)
        orderedRanking.sortByDescending {it.split("|")[1].toInt()}

        var usersList = mutableListOf(binding.player1, binding.player2, binding.player3, binding.player4, binding.player5, binding.player6, binding.player7, binding.player8, binding.player9, binding.player10)
        for (i in 0 until orderedRanking.size){
            val user = orderedRanking[i].split("|")
            usersList[i].gravity = Gravity.CENTER
            usersList[i].isVisible = true
            usersList[i].text = "${i+1} - ${user[0]}:    ${user[1]}"
        }

        binding.goBackButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, HomeFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
    }

    fun readFile(): String {
        var content = ""
        var rankingList = ("")
        val file = InputStreamReader(requireActivity().openFileInput("rankingFile.txt"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            rankingList += line
            content += line + "\n"
            line = br.readLine()
        }
        return (rankingList)
    }




}