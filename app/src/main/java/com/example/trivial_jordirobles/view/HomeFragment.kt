package com.example.trivial_jordirobles.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.example.trivial_jordirobles.R
import com.example.trivial_jordirobles.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val appContext = requireContext().applicationContext
        val anim = AnimationUtils.loadAnimation(context,R.anim.rotate_animation)

        binding.botonRedondo.isEnabled = true

        binding.botonRedondo.setOnClickListener {
            binding.logo.startAnimation(anim)

            Handler(Looper.getMainLooper()).postDelayed({
                parentFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainerView, GameModesFragment())
                    setReorderingAllowed(true)
                    addToBackStack("name")
                    commit()
                }
            }, 4000.toLong())
        }
        binding.secondButtonPanel.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, RankingFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
    }


}