package com.example.trivial_jordirobles.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.trivial_jordirobles.R
import com.example.trivial_jordirobles.databinding.FragmentCategoriesBinding

class CategoriesFragment : Fragment() {
    companion object{
        var topic = 0

    }
    lateinit var binding: FragmentCategoriesBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCategoriesBinding.inflate((layoutInflater))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.botonJugarTodo.setOnClickListener {
            topic = 1
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.botonDeporte.setOnClickListener {
            topic = 2
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.botonMusica.setOnClickListener {
            topic = 3
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.botonVideojuegos.setOnClickListener {
            topic = 4
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.botonCiencias.setOnClickListener {
            topic = 5
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.botonHistoria.setOnClickListener {
            topic = 6
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.botonArte.setOnClickListener {
            topic = 7
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.botonGeografia.setOnClickListener {
            topic = 8
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.botonCine.setOnClickListener {
            topic = 9
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.botonAtras.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, HomeFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
    }
}