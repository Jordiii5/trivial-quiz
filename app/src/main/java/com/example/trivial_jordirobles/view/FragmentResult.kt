package com.example.trivial_jordirobles.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.trivial_jordirobles.R
import com.example.trivial_jordirobles.databinding.FragmentResultatBinding
import com.example.trivial_jordirobles.view.CategoriesFragment.Companion.topic
import com.example.trivial_jordirobles.view.GameFragment.Companion.contadorpreguntascorrectas
import com.example.trivial_jordirobles.view.GameFragment.Companion.contadorpreguntasincorrectas
import com.example.trivial_jordirobles.view.GameFragment.Companion.totalScore
import java.io.OutputStream
import java.io.OutputStreamWriter

class FragmentResult: Fragment() {
    lateinit var binding: FragmentResultatBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentResultatBinding.inflate((layoutInflater))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.totalScoreTextView.text = "Puntuación: $totalScore"
        binding.respuestascorrectas.text = "Respuestas Correctas: $contadorpreguntascorrectas"
        binding.respuestasincorrectas.text = "Respuestas Inorrectas: $contadorpreguntasincorrectas"

        binding.usernameInsertTextview.isVisible = false
        binding.userEditText.isVisible = false
        binding.saveButton.isVisible = false
        binding.buttonNo.isVisible = false
        binding.guardarpartidapregunta.isVisible = false
        binding.textfinal.isVisible = true

        binding.buttonSi.isVisible = topic != 0
        binding.buttonNo.isVisible = topic != 0
        binding.textfinal.isVisible = topic == 0
        binding.guardarpartidapregunta.isVisible = topic != 0
        var gamemodeName =""
        when(topic){
            1-> gamemodeName ="ALEATORIO"
            2-> gamemodeName ="DEPORTE"
            3-> gamemodeName ="MUSICA"
            4-> gamemodeName ="VIDEOJUEGOS"
            5-> gamemodeName ="CIENCIAS"
            6-> gamemodeName ="HISTORIA"
            7-> gamemodeName ="ARTE"
            8-> gamemodeName ="GEOGRAFIA"
            9-> gamemodeName ="CINE"
        }


        binding.shareButton.setOnClickListener {
            val t1 = "TU PUNTUACIÓN HA SIDO: $totalScore\nEN EL MODO: $gamemodeName\nY HE HECHO $contadorpreguntascorrectas PREGUNTAS CORRECTAS Y $contadorpreguntasincorrectas INCORRECTAS"
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, t1)
            startActivity(Intent.createChooser(shareIntent, "Share via"))
        }
        binding.returnMenuButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, HomeFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.ranking.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, RankingFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.buttonNo.setOnClickListener {
            binding.usernameInsertTextview.isVisible = false
            binding.userEditText.isVisible = false
            binding.saveButton.isVisible = false
            binding.buttonSi.isVisible = false
            binding.buttonNo.isVisible = false
            binding.guardarpartidapregunta.isVisible = false
            binding.textfinal.isVisible = true
        }

        binding.buttonSi.setOnClickListener {
            binding.usernameInsertTextview.isVisible = true
            binding.userEditText.isVisible = true
            binding.saveButton.isVisible = true
            binding.buttonSi.isVisible = false
            binding.buttonNo.isVisible = false
            binding.guardarpartidapregunta.isVisible = false
            binding.textfinal.isVisible = false
            val appContext = requireContext().applicationContext

            binding.saveButton.setOnClickListener {
                var text = binding.userEditText.text
                if (";" in text || " " in text || "|" in text ||text.length > 10) {
                    Toast.makeText(appContext, "NOMBRE INVÁLIDO", Toast.LENGTH_SHORT).show()
                }

                else{
                    val file = OutputStreamWriter(requireActivity().openFileOutput("rankingFile.txt", Activity.MODE_APPEND))
                    file.appendLine("$text|$totalScore;")
                    file.flush()
                    file.close()

                    Toast.makeText(appContext, "PUNTUACIÓN GUARDADA", Toast.LENGTH_SHORT).show()
                    parentFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentContainerView, CategoriesFragment())
                        setReorderingAllowed(true)
                        addToBackStack("name")
                        commit()
                    }
                }

            }

        }
    }
}